return {
  -- set vim options here (vim.<first_key>.<second_key> =  value)
  opt = {
    relativenumber = false, -- sets vim.opt.relativenumber
    wrap = true, -- sets vim.opt.wrap
  }
}
