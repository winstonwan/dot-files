-- return {
--   -- set colorscheme
--   colorscheme = "dracula",
-- }
return {
  colorscheme = "dracula",

  plugins = {
    {
      "catppuccin/nvim",
      name = "catppuccin",
      config = function()
        require("catppuccin").setup {}
      end,
    },
    {
      "Mofiqul/dracula.nvim",
      name = "dracula",
      config = function()
        require("dracula").setup {}
      end,
    },
    {
      "kylechui/nvim-surround",
      version = "*", -- Use for stability; omit to use `main` branch for the latest features
      event = "VeryLazy",
      config = function()
        require("nvim-surround").setup({
          -- Configuration here, or leave empty to use defaults
        })
      end
    }
  },
}


