#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

echo "XDG Config home: $XDG_CONFIG_HOME"
git clone --depth 1 https://github.com/AstroNvim/AstroNvim $XDG_CONFIG_HOME/nvim
ln -s $DIR/xdg_config/nvim/lua/user $XDG_CONFIG_HOME/nvim/lua/user
