# remap prefix from 'C-b' to 'C-a'
unbind C-b
set-option -g prefix C-a
bind-key C-a send-prefix

# reload config file (change file location to your the tmux.conf you want to use)
bind r source-file ~/.config/tmux/tmux.conf

# Set new panes to open in current directory
bind c new-window -c "#{pane_current_path}"
# split panes using | and -
bind | split-window -h -c "#{pane_current_path}"
bind - split-window -v -c "#{pane_current_path}"
unbind '"'
unbind %

# vi keys in copy mode
setw -g mode-keys vi
unbind-key -T copy-mode-vi v
bind-key -T copy-mode-vi 'v' send -X begin-selection     # Begin selection in copy mode.
bind-key -T copy-mode-vi 'C-v' send -X rectangle-toggle  # Begin selection in copy mode.
bind-key -T copy-mode-vi 'y' send -X copy-selection      # Yank selection in copy mode.

# enable mouse support
set -g mouse on

# Start windows and pane numbering at 1 instead of 0
set -g base-index 1
set -g pane-base-index 1

# 256 Colors
set -g default-terminal "xterm-256color"
set -ga terminal-overrides ",*256col*:Tc"

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'christoomey/vim-tmux-navigator'
# Themepack color config
set -g @plugin 'jimeh/tmux-themepack'
set -g @themepack 'powerline/block/orange'

# Move status bar to top
set -g status-position top

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.tmux/plugins/tpm/tpm'

