#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

ln -s $DIR/home/tmux.conf ~/.tmux.conf
ln -s $DIR/home/vimrc ~/.vimrc
ln -s $DIR/home/gitconfig ~/.gitconfig

